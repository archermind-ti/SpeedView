## SpeedView


### 功能介绍
动态车速表、仪表。惊人的、强大的、多形状的 ⚡ ，你可以改变（颜色、长宽、形状、文本、字体......一切！！）

**Speedometers...**<br/>
<img src="images/speedometers1.gif" width="32%" />
<img src="images/speedometers2.gif" width="32%" />
<img src="images/speedometers3.gif" width="32%" />

**Gauges...**<br/>
<img src="images/speedometers5.gif" width="49%" />
<img src="images/speedometers4.gif" width="49%" />

### 安装教程

#### 方式一

 1. 下载[speedviewlib-v1.0.0.har](https://gitee.com/archermind-ti/SpeedView/releases/1.0.0)

 2. 将包放入需要的module的libs下

 3. 在module中添加对这两个har包的引用

    ```groovy
    dependencies {
        implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
        implementation project(":speedviewlib")
    	……
    }
    ```
#### 方式二
1. 在module的build.gradle中添加对`SpeedView`的依赖

   ```groovy
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
       ……
       implementation 'com.gitee.archermind-ti:speedviewlib:1.0.0-beta'
   }
   ```
2. 在project的build.gradle中添加`mavenCentral()`的引用

   ``` groovy
   allprojects {
       repositories {
           ……
           mavenCentral()
       }
   }
   ```

### 使用说明
选择速度计、仪表之一并将其添加到您的 layout.xml，这里我们使用 SpeedView。
``` java
<com.github.anastr.speedviewlib.SpeedView
        ohos:id="$+id:speedView"
        ohos:height="match_content"
        ohos:width="300vp"
        ohos:layout_alignment="horizontal_center"/>
```

对于所有车速表盘和仪表，下面的示例方法去平滑改变速度：

``` java
SpeedView speedometer  = (SpeedView) findComponentById(ResourceTable.Id_speedView);
// move to 50 Km/s
speedometer.speedTo(50)
```

默认情况下，上一个速度和新速度之间的速度变化持续时间为 2000 毫秒。 可以通过此方法传递持续时间：

``` java
// move to 50 Km/s with Duration = 4 sec
speedometer.speedTo(50, 4000)
```

由于默认开启 Tremble，指示器会自动围绕当前速度移动以向速度计添加一些真实感，您可以通过 app:sv_withTremble="false" 属性停止它或在您的代码中调用它：

``` java
speedometer.withTremble = false
```



### 版本迭代

* v1.0.0

# LICENSE
```

Copyright 2016 Anas Altair

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

```