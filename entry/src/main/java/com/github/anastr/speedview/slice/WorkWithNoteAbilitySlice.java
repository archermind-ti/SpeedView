package com.github.anastr.speedview.slice;

import com.github.anastr.speedview.ResourceTable;
import com.github.anastr.speedviewlib.SpeedView;
import com.github.anastr.speedviewlib.components.note.ImageNote;
import com.github.anastr.speedviewlib.components.note.Note;
import com.github.anastr.speedviewlib.components.note.TextNote;
import com.github.anastr.speedviewlib.util.ResUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;
import ohos.agp.text.*;
import ohos.agp.utils.Color;

import java.util.Locale;

public class WorkWithNoteAbilitySlice extends AbilitySlice {

    SpeedView speedView;
    Slider slider;
    Button ok;
    Text textSpeed;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_work_with_note);

        speedView = (SpeedView) findComponentById(ResourceTable.Id_speedView);
        slider = (Slider) findComponentById(ResourceTable.Id_slider);
        ok = (Button) findComponentById(ResourceTable.Id_ok);
        textSpeed = (Text) findComponentById(ResourceTable.Id_textSpeed);

        ok.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                speedView.speedTo(slider.getProgress());
            }
        });

        slider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                textSpeed.setText(String.format(Locale.getDefault(), "%d", i));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
        findComponentById(ResourceTable.Id_btnCenter).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Font type = ResUtil.getFont(getContext(),"lhandw.ttf");

                RichTextBuilder builder = new RichTextBuilder();
                builder.addText("Wait !");

                TextNote note = new TextNote(getApplicationContext(), builder.build())
                        .setPosition(Note.Position.CenterSpeedometer)
                        .setTextTypeFace(type)
                        .setTextSize(speedView.dpTOpx(20f));
                speedView.addNote(note, 1000);
            }
        });
        findComponentById(ResourceTable.Id_btnCenterIndicator).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                RichTextBuilder builder = new RichTextBuilder();
                builder.addText("Stop !!");
                TextNote note = new TextNote(getApplicationContext(), builder.build())
                        .setPosition(Note.Position.CenterIndicator)
                        .setTextTypeFace(Font.DEFAULT_BOLD)
                        .setTextSize(speedView.dpTOpx(13f));
                speedView.addNote(note, 1000);
            }
        });
        findComponentById(ResourceTable.Id_btnTopIndicator).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                RichTextBuilder builder = new RichTextBuilder();
                builder.addText("TOP");
                TextNote note = new TextNote(getApplicationContext(), builder.build())
                        .setPosition(Note.Position.TopIndicator)
                        .setAlign(Note.Align.Bottom)
                        .setTextSize(speedView.dpTOpx(10f));
                speedView.addNote(note, 1000);
            }
        });
        findComponentById(ResourceTable.Id_btnImageNote).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ImageNote imageNote = new ImageNote(getApplicationContext()
                        ,ResourceTable.Media_icon )
                        .setPosition(Note.Position.BottomIndicator);

                speedView.addNote(imageNote, 1000);
            }
        });
        findComponentById(ResourceTable.Id_btnSpannableString).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                TextForm textForm = new TextForm();
                int textSize = 9;

                RichTextBuilder builder = new RichTextBuilder();

                textForm.setTextSize(speedView.dpTOpx(textSize*1.7));
                textForm.setTextColor(Color.getIntColor("#64DD17"));
                builder.mergeForm(textForm);
                builder.addText("S");

                textForm.setTextColor(Color.getIntColor("#FF1744"));
                textForm.setTextSize(speedView.dpTOpx(textSize));
                builder.mergeForm(textForm);
                builder.addText("peed");

                textForm.setTextColor(Color.getIntColor("#AA00FF"));
                textForm.setTextSize(speedView.dpTOpx(textSize*1.4));
                builder.mergeForm(textForm);
                builder.addText("O");

                textForm.setTextColor(Color.getIntColor("#AA00FF"));
                textForm.setTextSize(speedView.dpTOpx(textSize));
                builder.mergeForm(textForm);
                builder.addText("meter");


                TextNote note = new TextNote(getApplicationContext(), builder.build())
                        .setBackgroundColor(new Color(Color.getIntColor("#EEFF41")))
                        .setPosition(Note.Position.QuarterSpeedometer);
                speedView.addNote(note, 1000);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (speedView != null) {
            speedView.onComponentUnboundFromWindow(speedView);
        }
    }
}
