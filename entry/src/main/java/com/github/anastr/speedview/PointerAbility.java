package com.github.anastr.speedview;

import com.github.anastr.speedview.slice.PointerAbilitySlice;
import com.github.anastr.speedviewlib.Gauge;
import com.github.anastr.speedviewlib.PointerSpeedometer;
import com.github.anastr.speedviewlib.util.OnSpeedChangeListener;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;

import java.util.Locale;

public class PointerAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(PointerAbilitySlice.class.getName());
    }
}
