package com.github.anastr.speedview.slice;

import com.github.anastr.speedview.ResourceTable;
import com.github.anastr.speedviewlib.Speedometer;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.AbsButton;
import ohos.agp.components.Checkbox;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;

import java.util.Locale;

public class TickAbilitySlice extends AbilitySlice {

    Speedometer speedometer;
    Checkbox withRotation;
    Slider seekBarTickNumbers, seekBarTickPadding;
    Text textTicks, textTickPadding;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_tick);

        speedometer = (Speedometer) findComponentById(ResourceTable.Id_speedometer);
        withRotation = (Checkbox) findComponentById(ResourceTable.Id_cb_withRotation);
        seekBarTickNumbers = (Slider) findComponentById(ResourceTable.Id_seekBarStartDegree);
        seekBarTickPadding = (Slider) findComponentById(ResourceTable.Id_seekBarTickPadding);
        textTicks = (Text) findComponentById(ResourceTable.Id_textTickNumber);
        textTickPadding = (Text) findComponentById(ResourceTable.Id_textTickPadding);

        speedometer.speedPercentTo(53);

        withRotation.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b) {
                speedometer.setTickRotation(b);
            }
        });

        seekBarTickNumbers.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                speedometer.setTickNumber(i);
                textTicks.setText(String.format(Locale.getDefault(), "%d", i));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        seekBarTickPadding.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                speedometer.setTickPadding((int) speedometer.dpTOpx(i));
                textTickPadding.setText(String.format(Locale.getDefault(), "%d vp", i));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (speedometer != null) {
            speedometer.onComponentUnboundFromWindow(speedometer);
        }
    }
}
