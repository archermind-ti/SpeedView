package com.github.anastr.speedview;

import com.github.anastr.speedview.slice.StartEndDegreeAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class StartEndDegreeAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(StartEndDegreeAbilitySlice.class.getName());
    }
}
