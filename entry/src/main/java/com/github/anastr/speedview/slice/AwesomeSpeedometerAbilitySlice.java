package com.github.anastr.speedview.slice;

import com.github.anastr.speedview.ResourceTable;
import com.github.anastr.speedviewlib.AwesomeSpeedometer;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;

import java.util.Locale;

public class AwesomeSpeedometerAbilitySlice extends AbilitySlice {

    AwesomeSpeedometer awesomeSpeedometer;
    Slider slider;
    Button speedTo, realSpeedTo, stop;
    Text textSpeed;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_awesome_speedometer);

        awesomeSpeedometer = (AwesomeSpeedometer) findComponentById(ResourceTable.Id_awesomeSpeedometer);
        slider = (Slider) findComponentById(ResourceTable.Id_slider);
        speedTo = (Button) findComponentById(ResourceTable.Id_speedTo);
        realSpeedTo = (Button) findComponentById(ResourceTable.Id_realSpeedTo);
        stop = (Button) findComponentById(ResourceTable.Id_stop);
        textSpeed = (Text) findComponentById(ResourceTable.Id_textSpeed);

        speedTo.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                awesomeSpeedometer.speedTo(slider.getProgress());
            }
        });
        realSpeedTo.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                awesomeSpeedometer.realSpeedTo(slider.getProgress());
            }
        });
        stop.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                awesomeSpeedometer.stop();
            }
        });
        slider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                textSpeed.setText(String.format(Locale.getDefault(), "%d", i));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        awesomeSpeedometer.onComponentUnboundFromWindow(awesomeSpeedometer);
    }
}
