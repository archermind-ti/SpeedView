package com.github.anastr.speedview.slice.provider;

import com.github.anastr.speedview.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;

import java.util.List;

public class MainItemProvider extends BaseItemProvider {
    private List<String> mData;
    private AbilitySlice mSlice;

    public MainItemProvider(List<String> data, AbilitySlice slice) {
        this.mData = data;
        this.mSlice = slice;
    }

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public Object getItem(int position) {
        if (mData != null && position >= 0 && position < mData.size()){
            return mData.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        final Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(mSlice).parse(ResourceTable.Layout_item_main_list, null, false);
        } else {
            cpt = convertComponent;
        }
        String item = mData.get(position);
        Text text = (Text) cpt.findComponentById(ResourceTable.Id_text);
        text.setText(item);
        return cpt;
    }
}
