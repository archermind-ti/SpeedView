package com.github.anastr.speedview;

import com.github.anastr.speedview.slice.ControlAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ControlAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ControlAbilitySlice.class.getName());
    }
}
