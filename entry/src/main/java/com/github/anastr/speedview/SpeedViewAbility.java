package com.github.anastr.speedview;

import com.github.anastr.speedview.slice.SpeedViewAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class SpeedViewAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SpeedViewAbilitySlice.class.getName());
    }
}
