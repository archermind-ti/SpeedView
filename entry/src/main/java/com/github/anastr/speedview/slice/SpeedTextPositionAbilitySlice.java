package com.github.anastr.speedview.slice;

import com.github.anastr.speedview.ResourceTable;
import com.github.anastr.speedviewlib.Speedometer;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ListDialog;

import java.util.ArrayList;
import java.util.List;

public class SpeedTextPositionAbilitySlice extends AbilitySlice {

    Speedometer speedometer;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_speed_text_position);

        speedometer = (Speedometer) findComponentById(ResourceTable.Id_speedometer);

        assert speedometer != null;
        speedometer.speedTo(40);
        Text spinner = (Text) findComponentById(ResourceTable.Id_spinner);

        List<String> categories = new ArrayList<>();
        categories.add("TOP_LEFT");
        categories.add("TOP_CENTER");
        categories.add("TOP_RIGHT");
        categories.add("LEFT");
        categories.add("CENTER");
        categories.add("RIGHT");
        categories.add("BOTTOM_LEFT");
        categories.add("BOTTOM_CENTER");
        categories.add("BOTTOM_RIGHT");

        ListDialog listDialog = new ListDialog(getContext(),ListDialog.SINGLE);
        listDialog.setItems(categories.toArray(new String[]{}));
        listDialog.setOnSingleSelectListener(new IDialog.ClickedListener() {
            @Override
            public void onClick(IDialog iDialog, int i) {
                speedometer.setSpeedTextPosition(Speedometer.Position.values()[i]);
                listDialog.hide();
                spinner.setText(categories.get(i));
            }
        });

        spinner.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                listDialog.show();
            }
        });

        spinner.setText(categories.get(7));

        findComponentById(ResourceTable.Id_unitUnderSpeedText).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (speedometer.isUnitUnderSpeedText())
                    speedometer.setUnitUnderSpeedText(false);
                else
                    speedometer.setUnitUnderSpeedText(true);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (speedometer != null) {
            speedometer.onComponentUnboundFromWindow(speedometer);
        }
    }
}
