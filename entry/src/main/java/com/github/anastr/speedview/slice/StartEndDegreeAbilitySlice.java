package com.github.anastr.speedview.slice;

import com.github.anastr.speedview.ResourceTable;
import com.github.anastr.speedviewlib.Speedometer;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;

import java.util.Locale;

public class StartEndDegreeAbilitySlice extends AbilitySlice {

    Speedometer speedometer;
    Slider seekBarStartDegree, seekBarEndDegree;;
    Text textStartDegree, textEndDegree;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_start_end_degree);

        speedometer = (Speedometer) findComponentById(ResourceTable.Id_speedometer);
        seekBarStartDegree = (Slider) findComponentById(ResourceTable.Id_seekBarStartDegree);
        seekBarEndDegree = (Slider) findComponentById(ResourceTable.Id_seekBarEndDegree);
        textStartDegree = (Text) findComponentById(ResourceTable.Id_textStartDegree);
        textEndDegree = (Text) findComponentById(ResourceTable.Id_textEndDegree);

        speedometer.speedPercentTo(50);

        seekBarStartDegree.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                int degree = i + 90;
                speedometer.setStartDegree(degree);
                textStartDegree.setText(String.format(Locale.getDefault(), "%d", degree));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
        seekBarEndDegree.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                int degree = i + 270;
                speedometer.setEndDegree(degree);
                textEndDegree.setText(String.format(Locale.getDefault(), "%d", degree));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (speedometer != null) {
            speedometer.onComponentUnboundFromWindow(speedometer);
        }
    }
}
