package com.github.anastr.speedview;

import com.github.anastr.speedview.slice.TubeSpeedometerAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class TubeSpeedometerAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(TubeSpeedometerAbilitySlice.class.getName());
    }
}
