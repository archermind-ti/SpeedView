package com.github.anastr.speedview;

import com.github.anastr.speedview.slice.DeluxeSpeedAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class DeluxeSpeedAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(DeluxeSpeedAbilitySlice.class.getName());
    }
}
