package com.github.anastr.speedview.slice;

import com.github.anastr.speedview.ResourceTable;
import com.github.anastr.speedviewlib.SpeedView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;

import java.util.Locale;

public class ControlAbilitySlice extends AbilitySlice {

    SpeedView speedView;
    Slider slider;
    TextField maxSpeed, speedometerWidth, speedTextSize, indicatorColor, centerCircleColor, lowSpeedColor, mediumSpeedColor, highSpeedColor;
    Checkbox withTremble;
    Text textSpeed;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_control);
        speedView = (SpeedView) findComponentById(ResourceTable.Id_awesomeSpeedometer);
        slider = (Slider) findComponentById(ResourceTable.Id_slider);
        textSpeed = (Text) findComponentById(ResourceTable.Id_textSpeed);
        maxSpeed = (TextField) findComponentById(ResourceTable.Id_maxSpeed);
        speedometerWidth = (TextField) findComponentById(ResourceTable.Id_speedometerWidth);
        speedTextSize = (TextField) findComponentById(ResourceTable.Id_speedTextSize);
        indicatorColor = (TextField) findComponentById(ResourceTable.Id_indicatorColor);
        centerCircleColor = (TextField) findComponentById(ResourceTable.Id_centerCircleColor);
        lowSpeedColor = (TextField) findComponentById(ResourceTable.Id_lowSpeedColor);
        mediumSpeedColor = (TextField) findComponentById(ResourceTable.Id_mediumSpeedColor);
        highSpeedColor = (TextField) findComponentById(ResourceTable.Id_highSpeedColor);
        withTremble = (Checkbox) findComponentById(ResourceTable.Id_withTremble);
        withTremble.setChecked(true);

        //设置光标圆形指示器大小
        int dp14 = speedView.dpTOpx(14);
        maxSpeed.setBubbleSize(dp14, dp14);
        speedometerWidth.setBubbleSize(dp14, dp14);
        speedTextSize.setBubbleSize(dp14, dp14);
        indicatorColor.setBubbleSize(dp14, dp14);
        centerCircleColor.setBubbleSize(dp14, dp14);
        lowSpeedColor.setBubbleSize(dp14, dp14);
        mediumSpeedColor.setBubbleSize(dp14, dp14);
        highSpeedColor.setBubbleSize(dp14, dp14);
        speedometerWidth.setBubbleSize(dp14, dp14);

        withTremble.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b) {
                speedView.setWithTremble(b);
            }
        });

        slider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                textSpeed.setText(String.format(Locale.getDefault(), "%d", i));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        setClick();
        speedView.speedTo(50);
    }

    private void setClick() {
        findComponentById(ResourceTable.Id_setSpeed).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                speedView.speedTo(slider.getProgress());
            }
        });
        findComponentById(ResourceTable.Id_setMaxSpeed).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                try {
                    int max = Integer.parseInt(maxSpeed.getText().toString());
                    slider.setMaxValue(max);
                    speedView.setMaxSpeed(max);
                } catch (Exception e) {
                    new ToastDialog(getContext()).setAlignment(LayoutAlignment.CENTER).setText(e.getMessage()).show();
                }
            }
        });

        findComponentById(ResourceTable.Id_setSpeedometerWidth).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                try {
                    float width = Float.parseFloat(speedometerWidth.getText().toString());
                    speedView.setSpeedometerWidth(width);
                } catch (Exception e) {
                    new ToastDialog(getContext()).setAlignment(LayoutAlignment.CENTER).setText(e.getMessage()).show();
                }
            }
        });
        findComponentById(ResourceTable.Id_setSpeedTextSize).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                try {
                    int size = Integer.parseInt(speedTextSize.getText());
                    speedView.setSpeedTextSize(size);
                } catch (Exception e) {
                    new ToastDialog(getContext()).setAlignment(LayoutAlignment.CENTER).setText(e.getMessage()).show();
                }
            }
        });
        findComponentById(ResourceTable.Id_setIndicatorColor).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                try {
                    speedView.setIndicatorColor(Color.getIntColor(indicatorColor.getText()));
                } catch (Exception e) {
                    new ToastDialog(getContext()).setAlignment(LayoutAlignment.CENTER).setText(e.getMessage()).show();
                }
            }
        });

        findComponentById(ResourceTable.Id_setCenterCircleColor).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                try {
                    speedView.setCenterCircleColor(Color.getIntColor(centerCircleColor.getText()));
                } catch (Exception e) {
                    new ToastDialog(getContext()).setAlignment(LayoutAlignment.CENTER).setText(e.getMessage()).show();
                }
            }
        });
        findComponentById(ResourceTable.Id_setLowSpeedColor).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                try {
                    speedView.setLowSpeedColor(Color.getIntColor(lowSpeedColor.getText()));
                } catch (Exception e) {
                    new ToastDialog(getContext()).setAlignment(LayoutAlignment.CENTER).setText(e.getMessage()).show();
                }
            }
        });
        findComponentById(ResourceTable.Id_setMediumSpeedColor).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                try {
                    speedView.setMediumSpeedColor(Color.getIntColor(mediumSpeedColor.getText()));
                } catch (Exception e) {
                    new ToastDialog(getContext()).setAlignment(LayoutAlignment.CENTER).setText(e.getMessage()).show();
                }
            }
        });
        findComponentById(ResourceTable.Id_setHighSpeedColor).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                try {
                    speedView.setHighSpeedColor(Color.getIntColor(highSpeedColor.getText()));
                } catch (Exception e) {
                    new ToastDialog(getContext()).setAlignment(LayoutAlignment.CENTER).setText(e.getMessage()).show();
                }
            }
        });

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (speedView != null) {
            speedView.onComponentUnboundFromWindow(speedView);
        }
    }
}
