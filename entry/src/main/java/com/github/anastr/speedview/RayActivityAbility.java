package com.github.anastr.speedview;

import com.github.anastr.speedview.slice.RayActivityAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class RayActivityAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(RayActivityAbilitySlice.class.getName());
    }
}
