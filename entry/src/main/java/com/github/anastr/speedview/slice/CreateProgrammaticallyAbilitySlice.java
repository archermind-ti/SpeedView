package com.github.anastr.speedview.slice;

import com.github.anastr.speedview.ResourceTable;
import com.github.anastr.speedviewlib.*;
import com.github.anastr.speedviewlib.components.Indicators.Indicator;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;

import java.util.Locale;
import java.util.Random;

public class CreateProgrammaticallyAbilitySlice extends AbilitySlice {

    DirectionalLayout rootSpeedometer;
    Speedometer speedometer;
    Slider slider;
    Button ok;
    Text textSpeed;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_create_programmatically);

        rootSpeedometer = (DirectionalLayout) findComponentById(ResourceTable.Id_root_speedometer);

        slider = (Slider) findComponentById(ResourceTable.Id_slider);
        ok = (Button) findComponentById(ResourceTable.Id_ok);
        textSpeed = (Text)findComponentById(ResourceTable.Id_textSpeed);

        ok.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (speedometer != null)
                    speedometer.speedTo(slider.getProgress());
            }
        });

        slider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                textSpeed.setText(String.format(Locale.getDefault(), "%d", i));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
        findComponentById(ResourceTable.Id_addRandomSpeedometer).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Random mad = new Random();
                switch (mad.nextInt(7)) {
                    case 0:
                        speedometer = new SpeedView(getContext());
                        break;
                    case 1:
                        speedometer = new DeluxeSpeedView(getContext());
                        break;
                    case 2:
                        speedometer = new AwesomeSpeedometer(getContext());
                        break;
                    case 3:
                        speedometer = new RaySpeedometer(getContext());
                        break;
                    case 4:
                        speedometer = new PointerSpeedometer(getContext());
                        break;
                    case 5:
                        speedometer = new TubeSpeedometer(getContext());
                        break;
                    case 6:
                        speedometer = new ImageSpeedometer(getContext());
                        speedometer.setIndicator(Indicator.Indicators.HalfLineIndicator);
                        speedometer.setIndicatorWidth(speedometer.dpTOpx(5f));
                        speedometer.setSpeedTextColor(Color.WHITE.getValue());
                        speedometer.setUnitTextColor(Color.WHITE.getValue());
                        ((ImageSpeedometer)speedometer).setImageSpeedometer(ResourceTable.Media_for_image_speedometer);
                        break;
                }
                rootSpeedometer.removeAllComponents();
                rootSpeedometer.addComponent(speedometer);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (speedometer != null) {
            speedometer.onComponentUnboundFromWindow(speedometer);
        }
    }
}
