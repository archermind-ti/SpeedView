package com.github.anastr.speedview.slice;

import com.github.anastr.speedview.ResourceTable;
import com.github.anastr.speedviewlib.Speedometer;
import com.github.anastr.speedviewlib.components.Indicators.ImageIndicator;
import com.github.anastr.speedviewlib.components.Indicators.Indicator;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ListDialog;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class WorkWithIndicatorAbilitySlice extends AbilitySlice {

    Speedometer speedometer;
    Text textWidth;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_work_with_indicator);

        speedometer = (Speedometer) findComponentById(ResourceTable.Id_speedometer);
        textWidth = (Text) findComponentById(ResourceTable.Id_textWidth);

        assert speedometer != null;
        speedometer.speedTo(40);

        Slider slider = (Slider) findComponentById(ResourceTable.Id_slider);
        assert slider != null;
        slider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                speedometer.setIndicatorWidth(speedometer.dpTOpx(i));
                textWidth.setText(String.format(Locale.getDefault(), "%dvp", i));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
        findComponentById(ResourceTable.Id_image).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                imageIndicator();
            }
        });


        Text spinner = (Text) findComponentById(ResourceTable.Id_spinner);

        List<String> categories = new ArrayList<>();
        categories.add("NoIndicator");
        categories.add("NormalIndicator");
        categories.add("NormalSmallIndicator");
        categories.add("TriangleIndicator");
        categories.add("SpindleIndicator");
        categories.add("LineIndicator");
        categories.add("HalfLineIndicator");
        categories.add("QuarterLineIndicator");
        categories.add("KiteIndicator");
        categories.add("NeedleIndicator");

        spinner.setText(categories.get(1));

        ListDialog listDialog = new ListDialog(getContext(), ListDialog.SINGLE);
        listDialog.setItems(categories.toArray(new String[]{}));
        listDialog.setOnSingleSelectListener(new IDialog.ClickedListener() {
            @Override
            public void onClick(IDialog iDialog, int i) {
                switch (i) {
                    case 0:
                        speedometer.setIndicator(Indicator.Indicators.NoIndicator);
                        break;
                    case 1:
                        speedometer.setIndicator(Indicator.Indicators.NormalIndicator);
                        break;
                    case 2:
                        speedometer.setIndicator(Indicator.Indicators.NormalSmallIndicator);
                        break;
                    case 3:
                        speedometer.setIndicator(Indicator.Indicators.TriangleIndicator);
                        break;
                    case 4:
                        speedometer.setIndicator(Indicator.Indicators.SpindleIndicator);
                        break;
                    case 5:
                        speedometer.setIndicator(Indicator.Indicators.LineIndicator);
                        break;
                    case 6:
                        speedometer.setIndicator(Indicator.Indicators.HalfLineIndicator);
                        break;
                    case 7:
                        speedometer.setIndicator(Indicator.Indicators.QuarterLineIndicator);
                        break;
                    case 8:
                        speedometer.setIndicator(Indicator.Indicators.KiteIndicator);
                        break;
                    case 9:
                        speedometer.setIndicator(Indicator.Indicators.NeedleIndicator);
                        break;
                }
                listDialog.hide();
                spinner.setText(categories.get(i));
            }
        });

        spinner.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                listDialog.show();
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    public void imageIndicator() {
        ImageIndicator imageIndicator = new ImageIndicator(getContext(), ResourceTable.Media_image_indicator1, (int) speedometer.dpTOpx(speedometer.getWidth() * 0.5), speedometer.dpTOpx(speedometer.getHeight() * 0.5));
        speedometer.setIndicator(imageIndicator);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (speedometer != null) {
            speedometer.onComponentUnboundFromWindow(speedometer);
        }
    }
}
