package com.github.anastr.speedview;

import com.github.anastr.speedview.slice.SpeedTextPositionAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class SpeedTextPositionAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SpeedTextPositionAbilitySlice.class.getName());
    }
}
