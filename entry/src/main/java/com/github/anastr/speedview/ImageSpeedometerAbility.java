package com.github.anastr.speedview;

import com.github.anastr.speedview.slice.ImageSpeedometerAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ImageSpeedometerAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ImageSpeedometerAbilitySlice.class.getName());
    }
}
