package com.github.anastr.speedview;

import com.github.anastr.speedview.slice.WorkWithIndicatorAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class WorkWithIndicatorAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(WorkWithIndicatorAbilitySlice.class.getName());
    }
}
