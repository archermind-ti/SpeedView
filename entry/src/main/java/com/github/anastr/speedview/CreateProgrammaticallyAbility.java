package com.github.anastr.speedview;

import com.github.anastr.speedview.slice.CreateProgrammaticallyAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class CreateProgrammaticallyAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(CreateProgrammaticallyAbilitySlice.class.getName());
    }
}
