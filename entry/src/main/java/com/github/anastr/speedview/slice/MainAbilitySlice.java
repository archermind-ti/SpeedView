package com.github.anastr.speedview.slice;

import com.github.anastr.speedview.*;
import com.github.anastr.speedview.lineargauge.ImageLinearGaugeAbility;
import com.github.anastr.speedview.lineargauge.ProgressiveGaugeAbility;
import com.github.anastr.speedview.slice.provider.MainItemProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;

import java.util.Arrays;

public class MainAbilitySlice extends AbilitySlice implements ListContainer.ItemClickedListener {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_list);
        String[] values = new String[]{
                "1. Speed View",
                "2. Deluxe Speed View",
                "3. Awesome Speedometer View",
                "4. Ray Speedometer View",
                "5. Pointer Speedometer",
                "6. Tube Speedometer",
                "7. Image Speedometer",
                "1.. ProgressiveGauge",
                "2.. ImageLinearGauge",
                "Work With Indicator",
                "Work With Note",
                "Create Speedometer Programmatically",
                "Work With Start and End Degree",
                "Work With Modes",
                "Speed Text Position",
                "Work With Ticks",
                "SpeedView with Recycler"
        };

        MainItemProvider mainItemProvider = new MainItemProvider(Arrays.asList(values), this);
        listContainer.setItemProvider(mainItemProvider);
        listContainer.setItemClickedListener(this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onItemClicked(ListContainer listContainer, Component component, int position, long id) {
        Intent intent = new Intent();
        Intent.OperationBuilder operation = new Intent.OperationBuilder();
        operation.withBundleName(getBundleName());
        switch (position) {
            case 0:
                operation.withAbilityName(SpeedViewAbility.class);
                break;
            case 1:
                operation.withAbilityName(DeluxeSpeedAbility.class);
                break;
            case 2:
                operation.withAbilityName(AwesomeSpeedometerAbility.class);
                break;
            case 3:
                operation.withAbilityName(RayActivityAbility.class);
                break;
            case 4:
                operation.withAbilityName(PointerAbility.class);
                break;
            case 5:
                operation.withAbilityName(TubeSpeedometerAbility.class);
                break;
            case 6:
                operation.withAbilityName(ImageSpeedometerAbility.class);
                break;
            case 7:
                operation.withAbilityName(ProgressiveGaugeAbility.class);
                break;
            case 8:
                operation.withAbilityName(ImageLinearGaugeAbility.class);
                break;
            case 9:
                operation.withAbilityName(WorkWithIndicatorAbility.class);
                break;
            case 10:
                operation.withAbilityName(WorkWithNoteAbility.class);
                break;
            case 11:
                operation.withAbilityName(CreateProgrammaticallyAbility.class);
                break;
            case 12:
                operation.withAbilityName(StartEndDegreeAbility.class);
                break;
            case 13:
                operation.withAbilityName(WorkWithModesAbility.class);
                break;
            case 14:
                operation.withAbilityName(SpeedTextPositionAbility.class);
                break;
            case 15:
                operation.withAbilityName(TickAbility.class);
                break;
            case 16:
                operation.withAbilityName(RecyclerAbility.class);
                break;
        }
        intent.setOperation(operation.build());
        startAbility(intent);
    }
}
