package com.github.anastr.speedview.slice;

import com.github.anastr.speedview.ResourceTable;
import com.github.anastr.speedviewlib.DeluxeSpeedView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.Locale;

public class DeluxeSpeedAbilitySlice extends AbilitySlice {

    DeluxeSpeedView deluxeSpeedView;
    Slider slider;
    Button ok;
    Text textSpeed;
    Checkbox withTremble, withEffects;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_deluxe_speed);
        deluxeSpeedView = (DeluxeSpeedView) findComponentById(ResourceTable.Id_deluxeSpeedView);
        slider = (Slider) findComponentById(ResourceTable.Id_slider);
        ok = (Button) findComponentById(ResourceTable.Id_ok);
        textSpeed = (Text) findComponentById(ResourceTable.Id_textSpeed);
        withTremble = (Checkbox) findComponentById(ResourceTable.Id_withTremble);
        withEffects = (Checkbox) findComponentById(ResourceTable.Id_withEffects);

        ok.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                deluxeSpeedView.speedTo(slider.getProgress());
            }
        });

        withTremble.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b) {
                deluxeSpeedView.setWithTremble(b);
            }
        });
        withEffects.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b) {
                deluxeSpeedView.setWithEffects(b);
            }
        });

        slider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                textSpeed.setText(String.format(Locale.getDefault(), "%d", i));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (deluxeSpeedView != null) {
            deluxeSpeedView.onComponentUnboundFromWindow(deluxeSpeedView);
        }
    }
}
