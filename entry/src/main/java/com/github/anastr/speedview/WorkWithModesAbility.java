package com.github.anastr.speedview;

import com.github.anastr.speedview.slice.WorkWithModesAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class WorkWithModesAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(WorkWithModesAbilitySlice.class.getName());
    }
}
