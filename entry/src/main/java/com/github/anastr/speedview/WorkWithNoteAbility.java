package com.github.anastr.speedview;

import com.github.anastr.speedview.slice.WorkWithNoteAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class WorkWithNoteAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(WorkWithNoteAbilitySlice.class.getName());
    }
}
