package com.github.anastr.speedview;

import com.github.anastr.speedview.slice.AwesomeSpeedometerAbilitySlice;
import com.github.anastr.speedviewlib.AwesomeSpeedometer;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;

import java.util.Locale;

public class AwesomeSpeedometerAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(AwesomeSpeedometerAbilitySlice.class.getName());
    }
}
