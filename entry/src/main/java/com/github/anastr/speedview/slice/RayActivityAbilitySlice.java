package com.github.anastr.speedview.slice;

import com.github.anastr.speedview.ResourceTable;
import com.github.anastr.speedviewlib.RaySpeedometer;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;

import java.util.Locale;

public class RayActivityAbilitySlice extends AbilitySlice {

    RaySpeedometer raySpeedometer;
    Slider seekBarSpeed, seekBarDegree, seekBarWidth;;
    Button ok;
    Text textSpeed, textDegree, textWidth;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_ray_activity);

        raySpeedometer = (RaySpeedometer) findComponentById(ResourceTable.Id_raySpeedometer);
        seekBarSpeed = (Slider) findComponentById(ResourceTable.Id_seekBarSpeed);
        seekBarDegree = (Slider) findComponentById(ResourceTable.Id_seekBarDegree);
        seekBarWidth = (Slider) findComponentById(ResourceTable.Id_seekBarWidth);
        ok = (Button) findComponentById(ResourceTable.Id_ok);
        textSpeed = (Text) findComponentById(ResourceTable.Id_textSpeed);
        textDegree = (Text) findComponentById(ResourceTable.Id_textDegree);
        textWidth = (Text) findComponentById(ResourceTable.Id_textWidth);

        ok.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                raySpeedometer.speedTo(seekBarSpeed.getProgress());
            }
        });
        seekBarSpeed.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                textSpeed.setText(String.format(Locale.getDefault(), "%d", i));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
        seekBarDegree.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                textDegree.setText(String.format(Locale.getDefault(), "%d", i));
                raySpeedometer.setDegreeBetweenMark(i);
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
        seekBarWidth.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                textWidth.setText(String.format(Locale.getDefault(), "%dvp", i));

                raySpeedometer.setMarkWidth((int) raySpeedometer.dpTOpx(i));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (raySpeedometer != null) {
            raySpeedometer.onComponentUnboundFromWindow(raySpeedometer);
        }
    }
}
