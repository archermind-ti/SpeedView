package com.github.anastr.speedview.lineargauge;

import com.github.anastr.speedview.lineargauge.slice.ImageLinearGaugeAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ImageLinearGaugeAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ImageLinearGaugeAbilitySlice.class.getName());
    }
}
