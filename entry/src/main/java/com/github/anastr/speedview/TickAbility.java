package com.github.anastr.speedview;

import com.github.anastr.speedview.slice.TickAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class TickAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(TickAbilitySlice.class.getName());
    }
}
