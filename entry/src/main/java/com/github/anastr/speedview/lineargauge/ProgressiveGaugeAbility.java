package com.github.anastr.speedview.lineargauge;

import com.github.anastr.speedview.lineargauge.slice.ProgressiveGaugeAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ProgressiveGaugeAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ProgressiveGaugeAbilitySlice.class.getName());
    }
}
