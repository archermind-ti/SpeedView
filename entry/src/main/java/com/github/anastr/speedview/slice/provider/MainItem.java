package com.github.anastr.speedview.slice.provider;

public class MainItem {

    private String itemName;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
