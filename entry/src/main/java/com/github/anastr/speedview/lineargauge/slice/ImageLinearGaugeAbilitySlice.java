package com.github.anastr.speedview.lineargauge.slice;

import com.github.anastr.speedview.ResourceTable;
import com.github.anastr.speedviewlib.ImageLinearGauge;
import com.github.anastr.speedviewlib.LinearGauge;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.Locale;

public class ImageLinearGaugeAbilitySlice extends AbilitySlice {

    ImageLinearGauge imageLinearGauge;
    Slider slider;
    Button ok;
    Text textSpeed;
    Checkbox checkboxOrientation;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_image_linear_gauge);

        imageLinearGauge = (ImageLinearGauge) findComponentById(ResourceTable.Id_gauge);
        slider = (Slider) findComponentById(ResourceTable.Id_slider);
        ok = (Button) findComponentById(ResourceTable.Id_ok);
        textSpeed = (Text) findComponentById(ResourceTable.Id_textSpeed);
        checkboxOrientation = (Checkbox) findComponentById(ResourceTable.Id_cb_orientation);

        ok.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                imageLinearGauge.speedTo(slider.getProgress());
            }
        });

        slider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                textSpeed.setText(String.format(Locale.getDefault(), "%d", i));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        checkboxOrientation.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b) {
                if (b)
                    imageLinearGauge.setOrientation(LinearGauge.Orientation.VERTICAL);
                else
                    imageLinearGauge.setOrientation(LinearGauge.Orientation.HORIZONTAL);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (imageLinearGauge != null) {
            imageLinearGauge.onComponentUnboundFromWindow(imageLinearGauge);
        }
    }
}
