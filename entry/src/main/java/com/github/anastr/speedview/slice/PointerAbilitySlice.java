package com.github.anastr.speedview.slice;

import com.github.anastr.speedview.ResourceTable;
import com.github.anastr.speedviewlib.Gauge;
import com.github.anastr.speedviewlib.PointerSpeedometer;
import com.github.anastr.speedviewlib.util.OnSpeedChangeListener;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;

import java.util.Locale;

public class PointerAbilitySlice extends AbilitySlice {

    PointerSpeedometer pointerSpeedometer;
    Slider seekBarSpeed;
    Button ok;
    Text textSpeed, textSpeedChange;;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_pointer);

        pointerSpeedometer = (PointerSpeedometer) findComponentById(ResourceTable.Id_pointerSpeedometer);
        seekBarSpeed = (Slider) findComponentById(ResourceTable.Id_seekBarSpeed);
        ok = (Button) findComponentById(ResourceTable.Id_ok);
        textSpeed = (Text) findComponentById(ResourceTable.Id_textSpeed);
        textSpeedChange = (Text) findComponentById(ResourceTable.Id_textSpeedChange);

        ok.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                pointerSpeedometer.speedTo(seekBarSpeed.getProgress());
            }
        });

        seekBarSpeed.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                textSpeed.setText(String.format(Locale.getDefault(), "%d", i));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
        pointerSpeedometer.setOnSpeedChangeListener(new OnSpeedChangeListener() {
            @Override
            public void onSpeedChange(Gauge gauge, boolean isSpeedUp, boolean isByTremble) {
                textSpeedChange.setText(String.format(Locale.getDefault(), "onSpeedChange %d"
                        , gauge.getCurrentIntSpeed()));
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (pointerSpeedometer != null) {
            pointerSpeedometer.onComponentUnboundFromWindow(pointerSpeedometer);
        }
    }
}
