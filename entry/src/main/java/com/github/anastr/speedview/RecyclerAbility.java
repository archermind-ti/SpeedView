package com.github.anastr.speedview;

import com.github.anastr.speedview.slice.RecyclerAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class RecyclerAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(RecyclerAbilitySlice.class.getName());
    }
}
