package com.github.anastr.speedview.lineargauge.slice;

import com.github.anastr.speedview.ResourceTable;
import com.github.anastr.speedviewlib.ProgressiveGauge;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;

import java.util.Locale;

public class ProgressiveGaugeAbilitySlice extends AbilitySlice {

    ProgressiveGauge progressiveGauge;
    Slider slider;
    Button ok;
    Text textSpeed;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_progressive_gauge);

        progressiveGauge = (ProgressiveGauge) findComponentById(ResourceTable.Id_gauge);
        slider = (Slider) findComponentById(ResourceTable.Id_slider);
        ok = (Button) findComponentById(ResourceTable.Id_ok);
        textSpeed = (Text) findComponentById(ResourceTable.Id_textSpeed);

        ok.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                progressiveGauge.speedTo(slider.getProgress());
            }
        });

        slider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                textSpeed.setText(String.format(Locale.getDefault(), "%d", i));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progressiveGauge != null) {
            progressiveGauge.onComponentUnboundFromWindow(progressiveGauge);
        }
    }
}
