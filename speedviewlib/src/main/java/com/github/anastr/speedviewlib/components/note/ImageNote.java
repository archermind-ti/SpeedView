package com.github.anastr.speedviewlib.components.note;

import com.github.anastr.speedviewlib.util.ResUtil;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;

/**
 * this Library build By Anas Altair
 * see it on <a href="https://github.com/anastr/SpeedView">GitHub</a>
 */
public class ImageNote extends Note<ImageNote> {

    private PixelMap image;
    private int width, height;
    private RectFloat imageRect = new RectFloat();
    private Paint notePaint = new Paint();

    /**
     * @param context  you can use {@code getApplicationContext()} method.
     * @param resource the image id.
     */
    public ImageNote(Context context, int resource) {
        this(context, ResUtil.getPixelMap(context, resource));
    }

    /**
     * @param context  you can use {@code getApplicationContext()} method.
     * @param resource the image id.
     * @param width    set custom width.
     * @param height   set custom height.
     * @throws IllegalArgumentException if {@code width <= 0 OR height <= 0}.
     */
    public ImageNote(Context context, int resource, int width, int height) {
        this(context, ResUtil.getPixelMap(context, resource)
                , width, height);
    }

    /**
     * @param context you can use {@code getApplicationContext()} method.
     * @param image   to display.
     */
    public ImageNote(Context context, PixelMap image) {
        this(context, image, image.getImageInfo().size.width, image.getImageInfo().size.height);
    }

    /**
     * @param context you can use {@code getApplicationContext()} method.
     * @param image   to display.
     * @param width   set custom width.
     * @param height  set custom height.
     * @throws IllegalArgumentException if {@code width <= 0 OR height <= 0}.
     */
    public ImageNote(Context context, PixelMap image, int width, int height) {
        super(context);
        notePaint.setAntiAlias(true);
        if (image == null)
            throw new IllegalArgumentException("image cannot be null.");
        this.image = image;
        this.width = width;
        this.height = height;
        if (width <= 0)
            throw new IllegalArgumentException("width must be bigger than 0");
        if (height <= 0)
            throw new IllegalArgumentException("height must be bigger than 0");
    }

    @Override
    public void build(int viewWidth) {
        noticeContainsSizeChange(this.width, this.height);
    }

    @Override
    protected void drawContains(Canvas canvas, float leftX, float topY) {
        imageRect = new RectFloat(leftX, topY, leftX + width, topY + height);
        PixelMapHolder imageHolder = new PixelMapHolder(image);
        canvas.drawPixelMapHolderRect(imageHolder, imageRect, notePaint);
    }
}
