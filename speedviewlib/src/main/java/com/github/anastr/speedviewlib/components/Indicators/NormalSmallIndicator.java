package com.github.anastr.speedviewlib.components.Indicators;


import ohos.agp.render.Canvas;
import ohos.agp.render.MaskFilter;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * this Library build By Anas Altair
 * see it on <a href="https://github.com/anastr/SpeedView">GitHub</a>
 */
public class NormalSmallIndicator extends Indicator<NormalSmallIndicator> {

    private Path indicatorPath = new Path();
    private float bottomY;

    public NormalSmallIndicator(Context context) {
        super(context);
        updateIndicator();
    }

    @Override
    protected float getDefaultIndicatorWidth() {
        return dpTOpx(12f);
    }

    @Override
    public float getTop() {
        return getViewSize()/5f + getPadding();
    }

    @Override
    public float getBottom() {
        return bottomY;
    }

    @Override
    public void draw(Canvas canvas, float degree) {
        canvas.save();
        canvas.rotate(90f + degree, getCenterX(), getCenterY());
        canvas.drawPath(indicatorPath, indicatorPaint);
        canvas.restore();
    }

    @Override
    protected void updateIndicator() {
        indicatorPath.reset();
        indicatorPath.moveTo(getCenterX(), getViewSize()/5f + getPadding());
        bottomY = getViewSize()*3f/5f + getPadding();
        indicatorPath.lineTo(getCenterX() - getIndicatorWidth(), bottomY);
        indicatorPath.lineTo(getCenterX() + getIndicatorWidth(), bottomY);
        RectFloat rectF = new RectFloat(getCenterX() - getIndicatorWidth(), bottomY - getIndicatorWidth()
                , getCenterX() + getIndicatorWidth(), bottomY + getIndicatorWidth());
        indicatorPath.addArc(rectF, 0f, 180f);

        indicatorPaint.setColor(new Color(getIndicatorColor()));
    }

    @Override
    protected void setWithEffects(boolean withEffects) {
        if (withEffects && !isInEditMode()) {
            indicatorPaint.setMaskFilter(new MaskFilter(15, MaskFilter.Blur.SOLID));
        }
        else {
            indicatorPaint.setMaskFilter(null);
        }
    }
}
