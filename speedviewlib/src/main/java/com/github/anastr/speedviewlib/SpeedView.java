package com.github.anastr.speedviewlib;

import com.github.anastr.speedviewlib.components.Indicators.NormalIndicator;
import com.github.anastr.speedviewlib.util.AttrSetUtils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

public class SpeedView extends Speedometer {

    private Path markPath = new Path();
    private Paint circlePaint = new Paint(),
            speedometerPaint = new Paint(),
            markPaint = new Paint();
    private RectFloat speedometerRect = new RectFloat();

    public SpeedView(Context context) {
        this(context, null);
    }

    public SpeedView(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public SpeedView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        circlePaint.setAntiAlias(true);
        speedometerPaint.setAntiAlias(true);
        markPaint.setAntiAlias(true);
        init();
        initAttributeSet(context, attrSet);
    }

    @Override
    protected void defaultGaugeValues() {
    }

    @Override
    protected void defaultSpeedometerValues() {
        super.setIndicator(new NormalIndicator(getContext()));
        super.setBackgroundCircleColor(0);
    }

    private void init() {
        speedometerPaint.setStyle(Paint.Style.STROKE_STYLE);
        markPaint.setStyle(Paint.Style.STROKE_STYLE);
        circlePaint.setColor(new Color(0xFF444444));
    }

    private void initAttributeSet(Context context, AttrSet attrs) {
        if (attrs == null) {
            return;
        }

        circlePaint.setColor(AttrSetUtils.getAttrSetColor(attrs, "sv_centerCircleColor", circlePaint.getColor()));
    }

    @Override
    protected void onSizeChange(int w, int h, int oldW, int oldH) {
        super.onSizeChange(w, h, oldW, oldH);
        updateBackgroundBitmap();
    }

    private void initDraw() {
        speedometerPaint.setStrokeWidth(getSpeedometerWidth());
        markPaint.setColor(new Color(getMarkColor()));
    }

    @Override
    public void onDrawComponent(Component component, Canvas canvas) {
        super.onDrawComponent(component, canvas);
        drawSpeedUnitText(canvas);

        drawIndicator(canvas);
        canvas.drawCircle(getSize() * .5f, getSize() * .5f, getWidthPa() / 12f, circlePaint);

        drawNotes(canvas);
    }

    @Override
    protected void updateBackgroundBitmap() {
        Canvas c = createBackgroundBitmapCanvas();
        initDraw();

        float markH = getViewSizePa() / 28f;
        markPath.reset();
        markPath.moveTo(getSize() * .5f, getCustomPadding());
        markPath.lineTo(getSize() * .5f, markH + getCustomPadding());
        markPaint.setStrokeWidth(markH / 3f);

        float risk = getSpeedometerWidth() * .5f + getCustomPadding();
        speedometerRect = new RectFloat(risk, risk, getSize() - risk, getSize() - risk);

        speedometerPaint.setColor(new Color(getHighSpeedColor()));
        c.drawArc(speedometerRect, new Arc(getStartDegree(), getEndDegree() - getStartDegree(), false), speedometerPaint);
        speedometerPaint.setColor(new Color(getMediumSpeedColor()));
        c.drawArc(speedometerRect, new Arc(getStartDegree()
                , (getEndDegree() - getStartDegree()) * getMediumSpeedOffset(), false), speedometerPaint);
        speedometerPaint.setColor(new Color(getLowSpeedColor()));
        c.drawArc(speedometerRect, new Arc(getStartDegree()
                , (getEndDegree() - getStartDegree()) * getLowSpeedOffset(), false), speedometerPaint);

        c.save();
        c.rotate(90f + getStartDegree(), getSize() * .5f, getSize() * .5f);
        float everyDegree = (getEndDegree() - getStartDegree()) * .111f;
        for (float i = getStartDegree(); i < getEndDegree() - (2f * everyDegree); i += everyDegree) {
            c.rotate(everyDegree, getSize() * .5f, getSize() * .5f);
            c.drawPath(markPath, markPaint);
        }
        c.restore();

        if (getTickNumber() > 0)
            drawTicks(c);
        else
            drawDefMinMaxSpeedPosition(c);
    }


    public int getCenterCircleColor() {
        return circlePaint.getColor().getValue();
    }

    /**
     * change the color of the center circle (if exist),
     * <b>this option is not available for all Speedometers</b>.
     * @param centerCircleColor new color.
     */
    public void setCenterCircleColor(int centerCircleColor) {
        circlePaint.setColor(new Color(centerCircleColor));
        if (!isAttachedToWindow())
            return;
        invalidate();
    }
}
