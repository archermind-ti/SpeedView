package com.github.anastr.speedviewlib.components.Indicators;

import com.github.anastr.speedviewlib.util.ResUtil;
import ohos.agp.render.Canvas;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.media.image.PixelMap;

/**
 * this Library build By Anas Altair
 * see it on <a href="https://github.com/anastr/SpeedView">GitHub</a>
 */
@SuppressWarnings("unused,WeakerAccess")
public class ImageIndicator extends com.github.anastr.speedviewlib.components.Indicators.Indicator<ImageIndicator> {

    private PixelMap bitmapIndicator;
    private int width, height;
    private RectFloat bitmapRect = new RectFloat();

    /**
     * create indicator from resources, the indicator direction must be up.<br>
     * center indicator position will be center of speedometer.
     *
     * @param context  you can use {@code getApplicationContext()}.
     * @param resource the image id.
     */
    public ImageIndicator(Context context, int resource) {
        this(context, ResUtil.getPixelMap(context, resource));
    }

    /**
     * create indicator from resources, the indicator direction must be up.<br>
     * center indicator position will be center of speedometer.
     *
     * @param context  you can use {@code getApplicationContext()}.
     * @param resource the image id.
     * @param width    the custom width of the indicator.
     * @param height   the custom height of the indicator.
     * @throws IllegalArgumentException if {@code width <= 0 OR height <= 0}.
     */
    public ImageIndicator(Context context, int resource, int width, int height) {
        this(context, ResUtil.getPixelMap(context, resource
                , width, height));
    }

    /**
     * create indicator from bitmap, the indicator direction must be up.<br>
     * center indicator position will be center of speedometer.
     *
     * @param context         you can use {@code getApplicationContext()}.
     * @param bitmapIndicator the indicator.
     */
    public ImageIndicator(Context context, PixelMap bitmapIndicator) {
        this(context, bitmapIndicator, bitmapIndicator.getImageInfo().size.width, bitmapIndicator.getImageInfo().size.height);
    }

    /**
     * create indicator from bitmap, the indicator direction must be up.<br>
     * center indicator position will be center of speedometer.
     *
     * @param context         you can use {@code getApplicationContext()}.
     * @param bitmapIndicator the indicator.
     * @param width           the custom width of the indicator.
     * @param height          the custom height of the indicator.
     * @throws IllegalArgumentException if {@code width <= 0 OR height <= 0}.
     */
    public ImageIndicator(Context context, PixelMap bitmapIndicator, int width, int height) {
        super(context);
        this.bitmapIndicator = bitmapIndicator;
        this.width = width;
        this.height = height;
        if (width <= 0)
            throw new IllegalArgumentException("width must be bigger than 0");
        if (height <= 0)
            throw new IllegalArgumentException("height must be bigger than 0");
    }

    @Override
    protected float getDefaultIndicatorWidth() {
        return 0f;
    }

    @Override
    public void draw(Canvas canvas, float degree) {
        canvas.save();
        canvas.rotate(90f + degree, getCenterX(), getCenterY());
        bitmapRect = new RectFloat(getCenterX() - (width / 2f), getCenterY() - (height / 2f)
                , getCenterX() + (width / 2f), getCenterY() + (height / 2f));
        canvas.drawPixelMapHolderRect(new PixelMapHolder(bitmapIndicator),  bitmapRect, indicatorPaint);
        canvas.restore();
    }

    @Override
    protected void updateIndicator() {
    }

    @Override
    protected void setWithEffects(boolean withEffects) {
    }

}
