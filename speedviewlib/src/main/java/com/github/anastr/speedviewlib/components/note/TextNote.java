package com.github.anastr.speedviewlib.components.note;


import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.text.RichText;
import ohos.agp.text.RichTextLayout;
import ohos.agp.text.SimpleTextLayout;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

/**
 * this Library build By Anas Altair
 * see it on <a href="https://github.com/anastr/SpeedView">GitHub</a>
 */
public class TextNote extends Note<TextNote> {

    private RichText noteText;
    private Paint notePaint = new Paint();
    private float textSize = notePaint.getTextSize();
    private RichTextLayout textLayout;

    /**
     * @param context  you can use {@code getApplicationContext()} method.
     * @param noteText text to display, support SpannableString and multi-lines.
     */
    public TextNote(Context context, RichText noteText) {
        super(context);
        notePaint.setAntiAlias(true);
        notePaint.setMultipleLine(true);
        if (noteText == null)
            throw new IllegalArgumentException("noteText cannot be null.");
        this.noteText = noteText;
        notePaint.setTextAlign(TextAlignment.LEFT);
    }

    @Override
    public void build(int viewWidth) {
        textLayout = new RichTextLayout(noteText, notePaint, new Rect(0, 0, 0, 0), viewWidth);
        int w = 0;
        for (int i = 0; i < textLayout.getLineCount(); i++)
            w = (int) Math.max(w, textLayout.getTextWidth(i));
        noticeContainsSizeChange(w, textLayout.getHeight());
    }

    @Override
    protected void drawContains(Canvas canvas, float leftX, float topY) {
        canvas.save();
        canvas.translate(leftX, topY);
        textLayout.drawText(canvas);
        canvas.restore();
    }

    public float getTextSize() {
        return textSize;
    }

    /**
     * set Text size.
     *
     * @param textSize in Pixel.
     * @return This Note object to allow for chaining of calls to set methods.
     */
    public TextNote setTextSize(int textSize) {
        this.textSize = textSize;
        notePaint.setTextSize(textSize);
        return this;
    }

    /**
     * to change font or text style.
     *
     * @param typeface new Typeface.
     * @return This Note object to allow for chaining of calls to set methods.
     */
    public TextNote setTextTypeFace(Font typeface) {
        notePaint.setFont(typeface);
        return this;
    }

    public Color getTextColor() {
        return notePaint.getColor();
    }

    /**
     * set text color.
     *
     * @param textColor new color.
     * @return This Note object to allow for chaining of calls to set methods.
     */
    public TextNote setTextColor(Color textColor) {
        notePaint.setColor(textColor);
        return this;
    }
}
