package com.github.anastr.speedviewlib.components.Indicators;

import ohos.agp.render.Canvas;
import ohos.agp.render.MaskFilter;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * this Library build By Anas Altair
 * see it on <a href="https://github.com/anastr/SpeedView">GitHub</a>
 */

public class NeedleIndicator extends Indicator {

    private Path indicatorPath = new Path();
    private Path circlePath = new Path();
    private Paint circlePaint =  new Paint();
    private float bottomY;

    public NeedleIndicator(Context context) {
        super(context);
        circlePaint.setAntiAlias(true);
        circlePaint.setStyle(Paint.Style.STROKE_STYLE);
        updateIndicator();
    }

    @Override
    protected float getDefaultIndicatorWidth() {
        return dpTOpx(12f);
    }

    @Override
    public float getBottom() {
        return bottomY;
    }

    @Override
    public void draw(Canvas canvas, float degree) {
        canvas.save();
        canvas.rotate(90f + degree, getCenterX(), getCenterY());
        canvas.drawPath(indicatorPath, indicatorPaint);
        canvas.drawPath(circlePath, circlePaint);
        canvas.restore();
    }

    @Override
    protected void updateIndicator() {
        indicatorPath.reset();
        circlePath.reset();
        indicatorPath.moveTo(getCenterX(), getPadding());
        bottomY = (float) (getIndicatorWidth() * Math.sin(Math.toRadians(260))) + getViewSize()*.5f + getPadding();
        float xLeft = (float) (getIndicatorWidth() * Math.cos(Math.toRadians(260))) + getViewSize()*.5f + getPadding();
        indicatorPath.lineTo(xLeft, bottomY);
        RectFloat rectF = new RectFloat(getCenterX() - getIndicatorWidth(), getCenterY() - getIndicatorWidth()
                , getCenterX() + getIndicatorWidth(), getCenterY() + getIndicatorWidth());
        indicatorPath.arcTo(rectF, 260, 20f);

        float circleWidth = getIndicatorWidth() *.25f;
        circlePath.addCircle(getCenterX(), getCenterY(), getIndicatorWidth() - circleWidth*.5f +.6f, Path.Direction.CLOCK_WISE);

        indicatorPaint.setColor(new Color(getIndicatorColor()));
        circlePaint.setColor(new Color(getIndicatorColor()));
        circlePaint.setStrokeWidth(circleWidth);
    }

    @Override
    protected void setWithEffects(boolean withEffects) {
        if (withEffects && !isInEditMode())
            indicatorPaint.setMaskFilter(new MaskFilter(15, MaskFilter.Blur.SOLID));
        else
            indicatorPaint.setMaskFilter(null);
    }
}
