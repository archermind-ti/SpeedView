package com.github.anastr.speedviewlib.components.Indicators;

import ohos.agp.render.Canvas;
import ohos.agp.render.MaskFilter;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * this Library build By Anas Altair
 * see it on <a href="https://github.com/anastr/SpeedView">GitHub</a>
 */
public class SpindleIndicator extends Indicator<SpindleIndicator> {

    private Path indicatorPath = new Path();

    public SpindleIndicator(Context context) {
        super(context);
        updateIndicator();
    }

    @Override
    protected float getDefaultIndicatorWidth() {
        return dpTOpx(16f);
    }

    @Override
    public float getTop() {
        return getViewSize()*.18f + getPadding();
    }

    @Override
    public void draw(Canvas canvas, float degree) {
        canvas.save();
        canvas.rotate(90f + degree, getCenterX(), getCenterY());
        canvas.drawPath(indicatorPath, indicatorPaint);
        canvas.restore();
    }

    @Override
    protected void updateIndicator() {
        indicatorPath.reset();
        indicatorPath.moveTo(getCenterX(), getCenterY());
        indicatorPath.quadTo(getCenterX() - getIndicatorWidth(), getViewSize()*.34f + getPadding()
                , getCenterX(), getViewSize()*.18f + getPadding());
        indicatorPath.quadTo(getCenterX() + getIndicatorWidth(), getViewSize()*.34f + getPadding()
                , getCenterX(), getCenterY());

        indicatorPaint.setColor(new Color(getIndicatorColor()));
    }

    @Override
    protected void setWithEffects(boolean withEffects) {
        if (withEffects && !isInEditMode()) {
            indicatorPaint.setMaskFilter(new MaskFilter(15, MaskFilter.Blur.SOLID));
        }
        else {
            indicatorPaint.setMaskFilter(null);
        }
    }
}
