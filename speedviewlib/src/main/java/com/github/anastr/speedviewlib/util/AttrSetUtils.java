package com.github.anastr.speedviewlib.util;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.Color;

import java.util.Optional;

/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class AttrSetUtils {

    private AttrSetUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * get attr color
     *
     * @param attrSet      AttrSet
     * @param attrName     String
     * @param defaultColor Color
     * @return Color int value
     */
    public static int getAttrSetColorInt(AttrSet attrSet, String attrName, int defaultColor) {
        if (attrSet == null || attrName == null || attrName.isEmpty()) {
            return defaultColor;
        }

        Optional<Attr> attrOptional = attrSet.getAttr(attrName);
        if (attrOptional == null || !attrOptional.isPresent()) {
            return defaultColor;
        }
        return attrOptional.get().getColorValue().getValue();
    }

    /**
     * get attr color
     *
     * @param attrSet      AttrSet
     * @param attrName     String
     * @param defaultColor Color
     * @return Color int value
     */
    public static Color getAttrSetColor(AttrSet attrSet, String attrName, Color defaultColor) {
        if (attrSet == null || attrName == null || attrName.isEmpty()) {
            return defaultColor;
        }

        Optional<Attr> attrOptional = attrSet.getAttr(attrName);
        if (attrOptional == null || !attrOptional.isPresent()) {
            return defaultColor;
        }
        return attrOptional.get().getColorValue();
    }

    /**
     * get dimension
     *
     * @param attrSet      AttrSet
     * @param attrName     String
     * @param defaultValue int
     * @return dimension value
     */
    public static int getDimension(AttrSet attrSet, String attrName, int defaultValue) {
        if (attrSet == null || attrName == null || attrName.isEmpty()) {
            return defaultValue;
        }
        Optional<Attr> attrOptional = attrSet.getAttr(attrName);
        if (attrOptional == null || !attrOptional.isPresent()) {
            return defaultValue;
        }
        return attrOptional.get().getDimensionValue();
    }

    /**
     * get float value
     *
     * @param attrSet      AttrSet
     * @param attrName     String
     * @param defaultValue float
     * @return float
     */
    public static float getFloat(AttrSet attrSet, String attrName, float defaultValue) {
        if (attrSet == null || attrName == null || attrName.isEmpty()) {
            return defaultValue;
        }
        Optional<Attr> attrOptional = attrSet.getAttr(attrName);
        if (attrOptional == null || !attrOptional.isPresent()) {
            return defaultValue;
        }
        return attrOptional.get().getFloatValue();
    }

    /**
     * get attr int
     *
     * @param attrSet    AttrSet
     * @param attrName   String
     * @param defaultInt int default
     * @return int
     */
    public static int getInt(AttrSet attrSet, String attrName, int defaultInt) {
        if (attrSet == null || attrName == null || attrName.isEmpty()) {
            return defaultInt;
        }
        Optional<Attr> attrOptional = attrSet.getAttr(attrName);
        if (attrOptional == null || !attrOptional.isPresent()) {
            return defaultInt;
        }
        return attrOptional.get().getIntegerValue();
    }

    /**
     * get attr boolean
     *
     * @param attrSet      AttrSet
     * @param attrName     String
     * @param defaultValue boolean default
     * @return boolean
     */
    public static boolean getBoolean(AttrSet attrSet, String attrName, boolean defaultValue) {
        if (attrSet == null || attrName == null || attrName.isEmpty()) {
            return defaultValue;
        }
        Optional<Attr> attrOptional = attrSet.getAttr(attrName);
        if (attrOptional == null || !attrOptional.isPresent()) {
            return defaultValue;
        }
        return attrOptional.get().getBoolValue();
    }

    /**
     * get string value
     *
     * @param attrSet      AttrSet
     * @param attrName     String
     * @param defaultValue String default
     * @return String
     */
    public static String getString(AttrSet attrSet, String attrName, String defaultValue) {
        if (attrSet == null || attrName == null || attrName.isEmpty()) {
            return defaultValue;
        }
        Optional<Attr> attrOptional = attrSet.getAttr(attrName);
        if (attrOptional == null || !attrOptional.isPresent()) {
            return defaultValue;
        }
        return attrOptional.get().getStringValue();
    }

    /**
     * get element
     *
     * @param attrSet  AttrSet
     * @param attrName String
     * @return Element
     */
    public static Optional<Element> getElement(AttrSet attrSet, String attrName) {
        if (attrSet == null || attrName == null || attrName.isEmpty()) {
            return Optional.empty();
        }
        Optional<Attr> attrOptional = attrSet.getAttr(attrName);
        if (attrOptional == null || !attrOptional.isPresent()) {
            return Optional.empty();
        }
        return Optional.of(attrOptional.get().getElement());
    }
}
