package com.github.anastr.speedviewlib;

import com.github.anastr.speedviewlib.util.AttrSetUtils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.text.Font;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;

import java.io.IOException;
import java.util.Optional;

public class ImageSpeedometer extends Speedometer {

    private PixelMapElement imageSpeedometer;

    public ImageSpeedometer(Context context) {
        this(context, null);
    }

    public ImageSpeedometer(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public ImageSpeedometer(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initAttributeSet(context, attrSet);
    }

    @Override
    protected void defaultSpeedometerValues() {
        setBackgroundCircleColor(0);
    }

    @Override
    protected void defaultGaugeValues() {

    }

    private void initAttributeSet(Context context, AttrSet attrs) {
        if (attrs == null)
            return;
        Optional<Element> imageOptional = AttrSetUtils.getElement(attrs, "sv_image");
        if (imageOptional.isPresent()) {
            imageSpeedometer = (PixelMapElement) imageOptional.get();
        }
    }

    @Override
    protected void onSizeChange(int w, int h, int oldW, int oldH) {
        super.onSizeChange(w, h, oldW, oldH);

        updateBackgroundBitmap();
    }

    @Override
    public void onDrawComponent(Component component, Canvas canvas) {
        super.onDrawComponent(component, canvas);

        drawSpeedUnitText(canvas);
        drawIndicator(canvas);
        drawNotes(canvas);
    }

    @Override
    protected void updateBackgroundBitmap() {
        Canvas c = createBackgroundBitmapCanvas();

        if (imageSpeedometer != null) {
            c.drawPixelMapHolderRect(new PixelMapHolder(imageSpeedometer.getPixelMap()), new RectFloat((int) getViewLeft() + getCustomPadding(), (int) getViewTop() + getCustomPadding()
                    , (int) getViewRight() - getCustomPadding(), (int) getViewBottom() - getCustomPadding()), new Paint());
        }
        drawTicks(c);
    }


    public PixelMapElement getImageSpeedometer() {
        return imageSpeedometer;
    }

    /**
     * set background speedometer image, Preferably be square.
     *
     * @param imageResource image id.
     * @see #setImageSpeedometer(PixelMapElement) ()
     */
    public void setImageSpeedometer(int imageResource) {
        try {
            Resource resourceObject = getResourceManager().getResource(imageResource);
            ImageSource imageSource = ImageSource.create(resourceObject, null);
            ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
            PixelMapElement pixelMapElement = new PixelMapElement(imageSource.createPixelmap(decodingOpts));
            setImageSpeedometer(pixelMapElement);
        } catch (IOException | NotExistException e) {
            e.printStackTrace();
        }
    }

    /**
     * set background speedometer image, Preferably be square.
     *
     * @param imageSpeedometer image drawable.
     * @see #setImageSpeedometer(int)
     */
    public void setImageSpeedometer(PixelMapElement imageSpeedometer) {
        this.imageSpeedometer = imageSpeedometer;
        updateBackgroundBitmap();
    }

    /**
     * this Speedometer doesn't use this method.
     *
     * @return {@code Color.TRANSPARENT} always.
     */
    @Deprecated
    @Override
    public int getLowSpeedColor() {
        return 0;
    }

    /**
     * this Speedometer doesn't use this method.
     *
     * @param lowSpeedColor nothing.
     */
    @Deprecated
    @Override
    public void setLowSpeedColor(int lowSpeedColor) {
    }

    /**
     * this Speedometer doesn't use this method.
     *
     * @return {@code Color.TRANSPARENT} always.
     */
    @Deprecated
    @Override
    public int getMediumSpeedColor() {
        return 0;
    }

    /**
     * this Speedometer doesn't use this method.
     *
     * @param mediumSpeedColor nothing.
     */
    @Deprecated
    @Override
    public void setMediumSpeedColor(int mediumSpeedColor) {
    }

    /**
     * this Speedometer doesn't use this method.
     *
     * @return {@code Color.TRANSPARENT} always.
     */
    @Deprecated
    @Override
    public int getHighSpeedColor() {
        return 0;
    }

    /**
     * this Speedometer doesn't use this method.
     *
     * @param highSpeedColor nothing.
     */
    @Deprecated
    @Override
    public void setHighSpeedColor(int highSpeedColor) {
    }

    /**
     * this Speedometer doesn't use this method.
     *
     * @param typeface nothing.
     */
    @Deprecated
    @Override
    public void setTextTypeface(Font typeface) {
    }

    /**
     * this Speedometer doesn't use this method.
     *
     * @return {@code 0} always.
     */
    @Deprecated
    @Override
    public float getTextSize() {
        return 0;
    }

    /**
     * this Speedometer doesn't use this method.
     *
     * @param textSize nothing.
     */
    @Deprecated
    @Override
    public void setTextSize(int textSize) {
    }

    /**
     * this Speedometer doesn't use this method.
     *
     * @return {@code 0} always.
     */
    @Deprecated
    @Override
    public int getTextColor() {
        return 0;
    }

    /**
     * this Speedometer doesn't use this method.
     *
     * @param textColor nothing.
     */
    @Deprecated
    @Override
    public void setTextColor(int textColor) {
    }
}
