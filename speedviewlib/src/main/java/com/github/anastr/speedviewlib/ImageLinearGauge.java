package com.github.anastr.speedviewlib;

import com.github.anastr.speedviewlib.util.AttrSetUtils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import java.util.Optional;

public class ImageLinearGauge extends LinearGauge {

    private PixelMapElement image;

    private int backColor = 0xffd6d7d7;

    public ImageLinearGauge(Context context) {
        this(context, null);
    }

    public ImageLinearGauge(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public ImageLinearGauge(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initAttributeSet(context, attrSet);
    }

    @Override
    protected void defaultGaugeValues() {
        super.setSpeedTextPosition(Position.CENTER);
        super.setUnitUnderSpeedText(true);
    }

    private void initAttributeSet(Context context, AttrSet attrs) {
        if (attrs == null)
            return;
        backColor = AttrSetUtils.getAttrSetColorInt(attrs, "sv_speedometerBackColor", backColor);
        Optional<Element> elementOptional = AttrSetUtils.getElement(attrs, "sv_image");
        if (elementOptional.isPresent()) {
            PixelMapElement pixelMapElement = (PixelMapElement) elementOptional.get();
            image = pixelMapElement;
        }
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        if (image == null
                || image.getPixelMap().getImageInfo().size.width == -1 || image.getPixelMap().getImageInfo().size.height == -1)
            return false;


        int w = EstimateSpec.getSize(widthMeasureSpec);
        int h = EstimateSpec.getSize(heightMeasureSpec);
        float imageW = (float) image.getPixelMap().getImageInfo().size.width;
        float imageH = (float) image.getPixelMap().getImageInfo().size.height;
        float view_w_to_h = (float) w / (float) h;
        float image_w_to_h = imageW / imageH;

        if (image_w_to_h > view_w_to_h)
            setEstSize(w, (int) (w * imageH / imageW));
        else
            setEstSize((int) (h * imageW / imageH), h);

        super.onEstimateSize(widthMeasureSpec, heightMeasureSpec);
        return true;
    }

    private void setEstSize(int width, int height) {
        setEstimatedSize(
                Component.EstimateSpec.getChildSizeWithMode(width, width, Component.EstimateSpec.PRECISE),
                Component.EstimateSpec.getChildSizeWithMode(height, height, Component.EstimateSpec.PRECISE)
        );
    }

    @Override
    protected void updateFrontAndBackBitmaps() {
        Canvas canvasBack = createBackgroundBitmapCanvas();
        Canvas canvasFront = createForegroundBitmapCanvas();

        if (image != null) {
            Paint paint = new Paint();
            paint.setColorFilter(new ColorFilter(backColor,BlendMode.SRC_IN));
            canvasBack.drawPixelMapHolderRect(new PixelMapHolder(image.getPixelMap()),new RectFloat(getCustomPadding(), getCustomPadding(), getEstimatedWidth() - getCustomPadding(), getEstimatedHeight() - getCustomPadding()),paint);

            paint.setColorFilter(null);
            canvasFront.drawPixelMapHolderRect(new PixelMapHolder(image.getPixelMap()),new RectFloat(getCustomPadding(), getCustomPadding(), getEstimatedWidth() - getCustomPadding(), getEstimatedHeight() - getCustomPadding()),paint);
        }
    }
}
