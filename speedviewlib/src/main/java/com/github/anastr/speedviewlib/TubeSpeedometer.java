package com.github.anastr.speedviewlib;

import com.github.anastr.speedviewlib.util.AttrSetUtils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.MaskFilter;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

public class TubeSpeedometer extends Speedometer {

    private Paint tubePaint = new Paint(), tubeBacPaint = new Paint();
    private RectFloat speedometerRect = new RectFloat();

    private boolean withEffects3D = true;


    public TubeSpeedometer(Context context) {
        this(context, null);
    }

    public TubeSpeedometer(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public TubeSpeedometer(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        tubePaint.setAntiAlias(true);
        tubeBacPaint.setAntiAlias(true);
        init();
        initAttributeSet(context, attrSet);
    }

    @Override
    protected void defaultGaugeValues() {
    }

    @Override
    protected void defaultSpeedometerValues() {
        super.setBackgroundCircleColor(0);
        super.setLowSpeedColor(0xff00BCD4);
        super.setMediumSpeedColor(0xffFFC107);
        super.setHighSpeedColor(0xffF44336);
        super.setSpeedometerWidth(dpTOpx(40f));
    }

    private void init() {
        tubePaint.setStyle(Paint.Style.STROKE_STYLE);
        tubeBacPaint.setStyle(Paint.Style.STROKE_STYLE);
        tubeBacPaint.setColor(new Color(0xff757575));
        tubePaint.setColor(new Color(getLowSpeedColor()));
    }

    private void initAttributeSet(Context context, AttrSet attrs) {
        if (attrs == null)
            return;

        tubeBacPaint.setColor(AttrSetUtils.getAttrSetColor(attrs, "sv_speedometerBackColor", tubeBacPaint.getColor()));
        withEffects3D = AttrSetUtils.getBoolean(attrs, "sv_withEffects3D", withEffects3D);
    }

    @Override
    protected void onSizeChange(int w, int h, int oldW, int oldH) {
        super.onSizeChange(w, h, oldW, oldH);
        updateEmboss();
        updateBackgroundBitmap();
    }

    private void updateEmboss() {
        if (!withEffects3D) {
            tubePaint.setMaskFilter(null);
            tubeBacPaint.setMaskFilter(null);
            return;
        }
        MaskFilter embossMaskFilter = new MaskFilter(new float[]{.5f, 1f, 1f}, .6f, 3f, pxTOdp(getSpeedometerWidth()) * .35f);
        tubePaint.setMaskFilter(embossMaskFilter);
        MaskFilter embossMaskFilterBac = new MaskFilter(
                new float[]{-.5f, -1f, 0f}, .6f, 1f, pxTOdp(getSpeedometerWidth()) * .35f);
        tubeBacPaint.setMaskFilter(embossMaskFilterBac);
    }

    private void initDraw() {
        tubePaint.setStrokeWidth(getSpeedometerWidth());
        byte section = getSection();
        if (section == LOW_SECTION)
            tubePaint.setColor(new Color(getLowSpeedColor()));
        else if (section == MEDIUM_SECTION)
            tubePaint.setColor(new Color(getMediumSpeedColor()));
        else
            tubePaint.setColor(new Color(getHighSpeedColor()));
    }

    @Override
    public void onDrawComponent(Component component, Canvas canvas) {
        super.onDrawComponent(component, canvas);
        initDraw();

        float sweepAngle = (getEndDegree() - getStartDegree()) * getOffsetSpeed();
        canvas.drawArc(speedometerRect, new Arc(getStartDegree(), sweepAngle, false), tubePaint);

        drawSpeedUnitText(canvas);
        drawIndicator(canvas);
        drawNotes(canvas);
    }

    @Override
    protected void updateBackgroundBitmap() {
        Canvas c = createBackgroundBitmapCanvas();
        tubeBacPaint.setStrokeWidth(getSpeedometerWidth());

        float risk = getSpeedometerWidth() *.5f + getCustomPadding();
        speedometerRect = new RectFloat(risk, risk, getSize() -risk, getSize() -risk);

        c.drawArc(speedometerRect, new Arc(getStartDegree(), getEndDegree()- getStartDegree(), false), tubeBacPaint);

        if (getTickNumber() > 0)
            drawTicks(c);
        else
            drawDefMinMaxSpeedPosition(c);
    }

    public int getSpeedometerBackColor() {
        return tubeBacPaint.getColor().getValue();
    }

    public void setSpeedometerBackColor(int speedometerBackColor) {
        tubeBacPaint.setColor(new Color(speedometerBackColor));
        updateBackgroundBitmap();
        invalidate();
    }

    @Override
    public void setLowSpeedColor(int lowSpeedColor) {
        super.setLowSpeedColor(lowSpeedColor);

    }

    public boolean isWithEffects3D() {
        return withEffects3D;
    }

    public void setWithEffects3D(boolean withEffects3D) {
        this.withEffects3D = withEffects3D;
        updateEmboss();
        updateBackgroundBitmap();
        invalidate();
    }
}
