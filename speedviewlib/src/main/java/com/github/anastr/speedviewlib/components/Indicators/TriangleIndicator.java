package com.github.anastr.speedviewlib.components.Indicators;


import com.github.anastr.speedviewlib.util.ResUtil;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.app.Context;

/**
 * this Library build By Anas Altair
 * see it on <a href="https://github.com/anastr/SpeedView">GitHub</a>
 */
public class TriangleIndicator extends Indicator<TriangleIndicator> {

    private Path indicatorPath = new Path();
    private float indicatorTop = 0;

    public TriangleIndicator(Context context) {
        super(context);
        updateIndicator();
    }

    @Override
    protected float getDefaultIndicatorWidth() {
        return dpTOpx(25f);
    }

    @Override
    public float getTop() {
        return indicatorTop;
    }

    @Override
    public float getBottom() {
        return indicatorTop + getIndicatorWidth();
    }

    @Override
    public void draw(Canvas canvas, float degree) {
        canvas.save();
        canvas.rotate(90f + degree, getCenterX(), getCenterY());
        canvas.drawPath(indicatorPath, indicatorPaint);
        canvas.restore();
    }

    @Override
    protected void updateIndicator() {
        indicatorPath = new Path();
        indicatorTop = getPadding() + getSpeedometerWidth() + dpTOpx(5);
        indicatorPath.moveTo(getCenterX(), indicatorTop);
        indicatorPath.lineTo(getCenterX() - getIndicatorWidth(), indicatorTop + getIndicatorWidth());
        indicatorPath.lineTo(getCenterX() + getIndicatorWidth(), indicatorTop + getIndicatorWidth());
        indicatorPath.moveTo(0f, 0f);

        int indicatorValue = getIndicatorColor();
        int endColor = Color.argb(0, ResUtil.red(indicatorValue), ResUtil.green(indicatorValue)
                ,  ResUtil.blue(indicatorValue));

        Point point1 = new Point(getCenterX(), indicatorTop);
        Point point2 = new Point(getCenterX(), indicatorTop + getIndicatorWidth());
        Point[] points = new Point[]{point1,point2};
        float[] floats = new float[]{};
        Color[] colors = new Color[]{new Color(getIndicatorColor()), new Color(endColor)};
        Shader linearGradient = new LinearShader(points,floats,colors,Shader.TileMode.CLAMP_TILEMODE);
        indicatorPaint.setShader(linearGradient, Paint.ShaderType.LINEAR_SHADER);
    }

    @Override
    protected void setWithEffects(boolean withEffects) {
        if (withEffects && !isInEditMode()) {
            indicatorPaint.setMaskFilter(new MaskFilter(15, MaskFilter.Blur.SOLID));
        }
        else {
            indicatorPaint.setMaskFilter(null);
        }
    }
}
